#pragma once
#include <iostream>
#include <string>
using namespace std;



class Student
{
public:
	void setStudent(string _name, int _id);
	void print();
	int calculateGrade();
	string name;
	int id;

private:
	
	int midTermExam;
	int finalExam;
};

