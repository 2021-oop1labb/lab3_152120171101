#include "Clock.h"

#include <iostream>
using namespace std;
const int timeZone=3;

void Clock::setTime(int hours, int minutes, int seconds)
{
	if (0 <= hours && hours < 24)
		hr = hours;
	else
		hr = 0;

	if (0 <= minutes && minutes < 60)
		min = minutes;
	else
		min = 0;
	
	if (0 <= seconds && seconds < 60)
		sec = seconds;
	else
		sec = 0;
}

void Clock::incrementHours()
{
	hr++;
	if (hr > 23)
		hr=0;
}

void Clock::incrementMinutes()
{
	min++;
	if (min > 59){
		min=0;
		incrementHours(); //increment hours
	}
}

void Clock::incrementSeconds()
{
	sec++;
	if (sec > 59){
		sec=0;
		incrementMinutes(); //increment minutes
	}
}

void Clock::incrementSeconds(int)
{
	sec++;
	if (sec > 59){
		sec=0;
		incrementMinutes(); //increment minutes
	}
}


bool Clock::equalTime(const Clock& otherClock) const
{
	return (hr == otherClock.hr
		&& min == otherClock.min
		&& sec == otherClock.sec);

}

void Clock::printTime()
{
	if (hr <10)
		cout << "0";
	cout << hr <<":";
	if (min <10)
		cout << "0";
	cout << min <<":";
	if (sec <10)
		cout << "0";
	cout << sec ;
}
bool equalClock(int clock1,int clock2) 
{
		if(clock1==clock2){
			cout<<endl;
			cout<<"clock1 and clock2 are equal"<<endl;
		return true;
	}
	else{
		cout<<"clock1 and clock2 are not equal"<<endl;
		return false; 
	}
		
}

